//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

#include "cbase.h"
#include "rotorwash.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

//===============================================
//===============================================
IMPLEMENT_SERVERCLASS_ST( CTERotorWash, DT_TERotorWash )
	SendPropVector( SENDINFO(m_vecDown), -1,	SPROP_COORD ),
	SendPropFloat( SENDINFO(m_flMaxAltitude), -1, SPROP_NOSCALE ),
END_SEND_TABLE()

static CTERotorWash g_TERotorWash( "RotorWash" );

CTERotorWash::CTERotorWash( const char *name ) : BaseClass( name )
{
	m_vecDown.Init();
	m_flMaxAltitude	= 0.0f;
}

CTERotorWash::~CTERotorWash( void )
{
}

void CTERotorWash::Create( IRecipientFilter& filter, float delay )
{
	engine->PlaybackTempEntity( filter, delay, (void *)this, GetServerClass()->m_pTable, GetServerClass()->m_ClassID );
}

void UTIL_RotorWash( const Vector &origin, const Vector &direction, float maxDistance )
{
	g_TERotorWash.m_vecOrigin		= origin;
	g_TERotorWash.m_vecDown			= direction;
	g_TERotorWash.m_flMaxAltitude	= maxDistance;

	CPVSFilter filter( origin );
	g_TERotorWash.Create( filter, 0.0f );
}

//===============================================
//===============================================
class CEnvRotorwash : public CPointEntity
{
public:
	DECLARE_CLASS( CEnvRotorwash, CPointEntity );

	DECLARE_DATADESC();

	// Input handlers
	void InputDoEffect( inputdata_t &inputdata );
};

LINK_ENTITY_TO_CLASS( env_rotorwash, CEnvRotorwash );

BEGIN_DATADESC( CEnvRotorwash )

	DEFINE_INPUTFUNC( FIELD_VOID, "DoEffect", InputDoEffect ),

END_DATADESC()

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
void CEnvRotorwash::InputDoEffect( inputdata_t &inputdata )
{
	UTIL_RotorWash( GetAbsOrigin(), Vector( 0, 0, -1 ), 512 );	
	UTIL_RotorWash( GetAbsOrigin(), Vector( 0, 0, -1 ), 512 );	
}

// ==============================================
//  Rotorwash entity
// ==============================================

class CRotorWashEmitter : public CBaseEntity
{
public:
	DECLARE_CLASS( CRotorWashEmitter, CBaseEntity );
	DECLARE_SERVERCLASS();
	DECLARE_DATADESC();

	void SetAltitude( float flAltitude ) { m_flAltitude = flAltitude; }
	void SetEmit( bool state ) { m_bEmit = state; }
	void Spawn ( void );
	void Precache( void );
	int ShouldTransmit( const CCheckTransmitInfo *pInfo );
	int UpdateTransmitState( void );

protected:

	CNetworkVar( bool, m_bEmit );
	CNetworkVar( float, m_flAltitude );
};

IMPLEMENT_SERVERCLASS_ST( CRotorWashEmitter, DT_RotorWashEmitter )
	SendPropFloat(SENDINFO(m_flAltitude), -1, SPROP_NOSCALE ),
END_SEND_TABLE()

LINK_ENTITY_TO_CLASS( env_rotorwash_emitter, CRotorWashEmitter );

BEGIN_DATADESC( CRotorWashEmitter )
	DEFINE_FIELD( 		m_bEmit, 		FIELD_BOOLEAN ),
	DEFINE_KEYFIELD( m_flAltitude, 	FIELD_FLOAT, "altitude" ),
END_DATADESC()

void CRotorWashEmitter::Spawn( void )
{
	Precache();
	BaseClass::Spawn();
	SetEmit( false );
}

void CRotorWashEmitter::Precache( void )
{
	PrecacheMaterial( "effects/splashwake3" );
}

int CRotorWashEmitter::ShouldTransmit( const CCheckTransmitInfo *pInfo )
{
	if ( GetParent() )
	{
		return GetParent()->ShouldTransmit( pInfo );
	}

	return FL_EDICT_PVSCHECK;
}

int CRotorWashEmitter::UpdateTransmitState( void )
{
	if ( GetParent() )
	{
		return SetTransmitState( FL_EDICT_FULLCHECK );
	}

	return SetTransmitState( FL_EDICT_PVSCHECK );
}

//-----------------------------------------------------------------------------
// Purpose: 
// Input  : &localOrigin - 
//			&localAngles - 
//			*pOwner - 
//			flAltitude - 
// Output : CBaseEntity
//-----------------------------------------------------------------------------
CBaseEntity *CreateRotorWashEmitter( const Vector &localOrigin, const QAngle &localAngles, CBaseEntity *pOwner, float flAltitude )
{
	CRotorWashEmitter *pEmitter = (CRotorWashEmitter *) CreateEntityByName( "env_rotorwash_emitter" );

	if ( pEmitter == NULL )
		return NULL;

	pEmitter->SetAbsOrigin( localOrigin );
	pEmitter->SetAbsAngles( localAngles );
	pEmitter->FollowEntity( pOwner );

	pEmitter->SetAltitude( flAltitude );
	pEmitter->SetEmit( false );

	return pEmitter;
}